#!/bin/bash
set -e

here=$(pwd)

[[ ! -d "app/src" ]] && echo "Initalizing submodule..." && git submodule init && git submodule update

[[ ! -f "app/src/config.ts" ]] && echo "Copying config..." && cp config.ts app/src/config.ts
[[ ! -f "app/src/styles.ts" ]] && echo "Copying styles config..." && cp app/src/styles.ts.sample app/src/styles.ts

[[ -f ".env.local" && ! -f "app/.env.local" ]] && echo "Copying local env..." && cp .env.local app/.env.local

echo "Copying data..."
cp -R public/* app/public/

echo "Initializing npm"
cd app
npm i

echo ""
echo "---"

echo "Ready! :)"
echo "You should now cd into app and use 'npm run start'"
