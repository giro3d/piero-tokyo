import { Configuration } from '@/types/Configuration';

const config: Configuration = {
    default_crs: 'EPSG:3857',
    // Switch-on experimental features
    enabled_features: ['measurements'],
    camera: {
        position: { crs: 'EPSG:4326', x: 139.7974316107759, y: 35.632123632361846, z: 4000 },
    },
    basemap: {
        center: { crs: 'EPSG:4326', x: 139.7974316107759, y: 35.632123632361846 },
        size: [100000, 100000],
        colormap: {
            min: 0,
            max: 100,
            ramp: 'RdYlBu',
        },
        layers: [
            {
                type: 'color',
                name: 'Geology',
                visible: false,
                source: {
                    type: 'wmts',
                    projection: 'EPSG:3857',
                    format: 'image/png',
                    layer: 'g',
                    url: 'https://gbank.gsj.jp/seamless/tilemap/detailed/WMTSCapabilities.xml',
                }
            },
            {
                type: 'color',
                name: 'OpenStreetMap',
                visible: true,
                source: {
                    type: 'osm',
                },
            },
        ],
    },
    pointcloud: {
        min: 160,
        max: 300,
        ramp: 'Viridis',
    },
    analysis: {
        cross_section: {
            pivot: { crs: 'EPSG:4326', x: 139.7974316107759, y: 35.632123632361846 },
            orientation: 0,
        },
        clipping_box: {
            center: { crs: 'EPSG:4326', x: 139.7974316107759, y: 35.632123632361846, z: 0 },
            size: {
                x: 200,
                y: 200,
                z: 200,
            },
            floor_preset: {
                altitude: 48,
                size: 4,
                floor: 1,
            },
        },
    },
    datasets: [],
    overlays: [
        {
            name: 'Orthophoto (GeoTIFF)',
            visible: false,
            source: {
                type: 'cog',
                projection: 'EPSG:3857',
                url: 'https://3d.oslandia.com/giro3d/rasters/Tokyo.orthophoto.tiff',
            }
        },
        {
            name: 'Child Education facilities (教育・子どもの施設)',
            visible: false,
            source: {
                type: 'geojson',
                projection: 'EPSG:4326',
                style: {
                    point: {
                        radius: 10,
                        fill: {
                            color: 'red',
                        },
                        stroke: {
                            color: 'black',
                            width: 2,
                        }
                    }
                },
                url: 'public/kyoikushisetsu.geojson'
            }
        }
    ],
    bookmarks: [],
};

export default config;
